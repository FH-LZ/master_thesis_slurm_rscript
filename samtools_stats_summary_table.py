# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import pandas as pd
import glob
import os 
import sys

# have to be executed one folder before the stats files 
# read the file and get lines start with SN 
# split the list separated by \t and save it into
# list of lists 
def read_stats_save_to_list_of_lists(filename): 

    list_of_lists = []

    with open(filename) as f_li:
        data_li = f_li.readlines()
    for line_li in data_li:
        if line_li.startswith('SN'):
            #print(line_li)
            stripped_line = line_li.strip()
            line_list = stripped_line.split("\t")
            list_of_lists.append(line_list)
    return list_of_lists


# %%
# testing the read_stats_save_to_list_of_lists

# list_of_lists = read_stats_save_to_list_of_lists('./GRCm39_samtools_stats_plots/SRR1044037_cutadapt_m20_GRCm39_hisat2_statsForPlot.stats') 
# print (list_of_lists)
# list_of_lists_2 = read_stats_save_to_list_of_lists('.\\NCMIM37\\SRR1044039_cutadapt_m20_full_polyA_NCBIM37_hisat2_statsForPlot.stats') 
# print (list_of_lists_2)


# %%
def listify(arg):
    return arg if isinstance(arg, list) else [arg]


# %%
def from_stats_files_to_dataframe (stats_files_input):

    df_all_header = pd.DataFrame()
    for a_file in stats_files_input:
        list_of_lists = read_stats_save_to_list_of_lists(a_file)
        df_a_file = pd.DataFrame(list_of_lists)
        # select out the 2nd and 3rd columns 
        df_a_file_col12 = df_a_file.iloc[:, 1:3]
        # transpose the dataframe 
        df_a_file_col12_T = df_a_file_col12.T
        # set 0 row as header 
        headers = df_a_file_col12_T.iloc[0]
        # a new dataframe with header and values 
        #print (list(a_file.split("\\")))
        a_file_split = list(a_file.split("\\"))
        index_a_file = listify(a_file_split[-1])
    
        # first time 
        if df_all_header.empty: 
            df_all_header  = pd.DataFrame(df_a_file_col12_T.values[1:], index = index_a_file, columns = headers)
        # print (df_all_header)
        else: 
            df_a_file_header  = pd.DataFrame(df_a_file_col12_T.values[1:], index = index_a_file, columns = headers)
            df_all_header = df_all_header.append (df_a_file_header)

    return df_all_header

    


# %%
# convert column "a" of a DataFrame
def conver_dataframe_to_numeric(data_frame): 

    for column in data_frame: 
        data_frame[column] = pd.to_numeric(data_frame[column])

    return data_frame



# %%
def remove_columns_all_0 (data_frame_num):
    
    return data_frame_num.loc[:, (data_frame_num != 0).any(axis=0)]


# %%

# have to be executed one folder before the stats files
# will go into the folders and look for stats file extention 
# input path 
# output a txt files of all none zero columns 


def main (path):
    # save all file names with .stats extention into a list 
    #path = "."
    stats_files = glob.glob (path + "/**/*.stats", recursive = True) 
    #print (stats_files)
    data_frame = from_stats_files_to_dataframe (stats_files)
    #print (data_frame)
    data_frame_num = conver_dataframe_to_numeric(data_frame)
    df = remove_columns_all_0(data_frame_num)
    #print (df)
    # write to a text file 
    base_filename = 'samtools_stats_summary.txt'
    with open(os.path.join(path, base_filename),'w') as outfile:
        df.to_string(outfile, header=True, index=True)

    # calculate the relative values for each columns:
    df ["mapped_reads_ratio"] =  df["reads mapped:"]/df["raw total sequences:"]*100
    df ["mapped_bases_ratio"] =  df["bases mapped:"]/df["total length:"]*100
    df['mapped_reads_ratio'].round(decimals = 3)
    df['mapped_bases_ratio'].round(decimals = 3)
    # move the mapped ration to front 

    
    df = df[ ['mapped_bases_ratio'] + [ col for col in df.columns if col != 'mapped_bases_ratio'] ]
    df = df[ ['mapped_reads_ratio'] + [ col for col in df.columns if col != 'mapped_reads_ratio'] ]
    # save the sorted ratio file 
    base_filename_retio = 'samtools_stats_summary_ratio.csv'
    with open(os.path.join(path, base_filename_retio),'w') as outfile_ratio:
        df.to_csv(outfile_ratio, header=True, index=True, sep = ';')


    # sort based on the 
    final_df = df.sort_values(by=['mapped_bases_ratio'], ascending=False)
    # save the sorted ratio file 
    base_filename_retio_sorted = 'samtools_stats_summary_ratio_sorted.txt'
    with open(os.path.join(path, base_filename_retio_sorted),'w') as outfile_ratio_sorted:
        final_df.to_string(outfile_ratio_sorted, header=True, index=True)

    return None


if __name__ == "__main__":
    path = str(sys.argv[1])
    main(path)





# %%

# main_function (path='D:\Lijuan\Documents\_Diplome_FH\copy_files_from_server\samtools_stat_plot')


# %%



