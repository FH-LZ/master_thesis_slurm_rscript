---
title: "Gene Set Enrichment Analysis with ClusterProfiler"
author: "Li"
date: "5/19/2019"
output:
  html_document:
    df_print: paged
df_print: paged
---

This R Notebook describes the implementation of gene set enrichment analysis (GSEA) using the clusterProfiler package. For more information please see the full documentation here: https://bioconductor.org/packages/release/bioc/vignettes/clusterProfiler/inst/doc/clusterProfiler.html


# Install and load required packages
```{r, message=F, warning=F}
#BiocManager::install("clusterProfiler", version = "3.8")
#BiocManager::install("pathview")
#BiocManager::install("enrichplot")
#BiocManager::install("DOSE")
#install.packages("ggnewscale")
#install.packages("ggridges")
#install.packages("europepmc")

library(clusterProfiler)
library(enrichplot)
library(pathview)
# we use ggplot2 to add x axis labels (ex: ridgeplot)
library(ggplot2)
library(DOSE)
library(ggnewscale)
library(ggridges)
library(europepmc)
#library(org.Mm.eg.db)

# SET THE DESIRED ORGANISM HERE
organism = "org.Mm.eg.db"
#BiocManager::install(organism, character.only = TRUE)
library(organism, character.only = TRUE)

```

# Annotations
I'm using *D melanogaster* data, so I install and load the annotation "org.Dm.eg.db" below. See all annotations available here: http://bioconductor.org/packages/release/BiocViews.html#___OrgDb (there are 19 presently available). 

```{r, message=F, warning=F}
# SET THE DESIRED ORGANISM HERE
#organism = "org.Mm.eg.db"
#BiocManager::install(organism, character.only = TRUE)
#library(organism, character.only = TRUE)
```

#Prepare Input
```{r}
# reading in data from deseq2
df = read.csv("C:/Lijuan/rattei_bioinf_master/_Diplome_FH/copy_files_from_server/SFB-RNA-data/DESeq2_edger_limma/top1372_IL4_DESeq2_tran.csv", header=TRUE)

#identiry rows from -1 to 1
#identify_rows <- df2$log2FoldChange >= -1 & df2$log2FoldChange <= 1

# exclude the -1 and 1 values
#df <- df2[!identify_rows, ] 

# we want the log2 fold change 
original_gene_list <- df$log2FoldChange
#original_gene_list
# name the vector
names(original_gene_list) <- df$X

unpub_il4_deseq2_threshhold1 <- df$X
length(unpub_il4_deseq2_threshhold1)
save(unpub_il4_deseq2_threshhold1, file="unpub_il4_deseq2_threshhold1.RData")


# omit any NA values 
gene_list<-na.omit(original_gene_list)

# sort the list in decreasing order (required for clusterProfiler)
gene_list = sort(gene_list, decreasing = TRUE)

length (gene_list)
#gene_list
```

## Gene Set Enrichment
Params:  
  
**keyType** This is the source of the annotation (gene ids). The options vary for each annotation. In the example of *org.Dm.eg.db*, the options are:   
  
"ACCNUM"       "ALIAS"        "ENSEMBL"      "ENSEMBLPROT"  "ENSEMBLTRANS" "ENTREZID"      
"ENZYME"       "EVIDENCE"     "EVIDENCEALL"  "FLYBASE"      "FLYBASECG"    "FLYBASEPROT"   
"GENENAME"     "GO"           "GOALL"        "MAP"          "ONTOLOGY"     "ONTOLOGYALL"   
"PATH"         "PMID"         "REFSEQ"       "SYMBOL"       "UNIGENE"      "UNIPROT"  
  
Check which options are available with the `keytypes` command, for example `keytypes(org.Dm.eg.db)`. 
  
**ont** one of "BP", "MF", "CC" or "ALL"  
Biological Processes (BP), Molecular Function (MF), or Cellular Components (CC).
**nPerm** permutation numbers, the higher the number of permutations you set, the more accurate your results is, but it will also cost longer time for running permutation.  
**minGSSize** minimal size of each geneSet for analyzing.   
**maxGSSize** maximal size of genes annotated for testing.   
**pvalueCutoff** pvalue Cutoff.   
**pAdjustMethod** one of "holm", "hochberg", "hommel", "bonferroni", "BH", "BY", "fdr", "none" 

```{r}
#gse_old <- gseGO(geneList=gene_list, 
#             ont ="ALL", 
#             keyType = "SYMBOL", 
#             nPerm = 10000, 
#             minGSSize = 3, 
#             maxGSSize = 800, 
#             pvalueCutoff = 0.05, 
#             verbose = TRUE, 
#             OrgDb = organism, 
#             pAdjustMethod = "none")
```


```{r}
gse <- gseGO(geneList=gene_list, 
             ont ="ALL", 
             keyType = "SYMBOL", 
             nPermSimple = 10000, 
             minGSSize = 3, 
             maxGSSize = 800, 
             pvalueCutoff = 0.05, 
             verbose = TRUE, 
             OrgDb = organism, 
             pAdjustMethod = "none")


```

# Output
##Table of results

```{r}
#head(gse)
gse_df <- as.data.frame(gse)
#head(gse_df)
sig_gse_df <- gse_df[gse_df$p.adjust <= 0.05, ]

str(sig_gse_df)
sig_gse_df_sorted <- sig_gse_df[order(sig_gse_df$p.adjust),]
sig_gse_df_sorted
#write.csv(sig_gse_df_sorted,"sig_gse_df_sorted_IL4.csv")
#top 30 
sig_gse_df_head_30 <- head(sig_gse_df[,c("Description", "setSize", "p.adjust", "enrichmentScore")], 30)

GO_list_IL4_DESeq2_30 <- sig_gse_df_head_30$Description
length(GO_list_IL4_DESeq2_30)
save(GO_list_IL4_DESeq2_30, file="GO_list_IL4_DESeq2_30.RData")
# top 50 
sig_gse_df_head_50 <- head(sig_gse_df[,c("Description", "setSize", "p.adjust", "enrichmentScore")], 50)

GO_list_IL4_DESeq2_50 <- sig_gse_df_head_50$Description
length(GO_list_IL4_DESeq2_50)
save(GO_list_IL4_DESeq2_50, file="GO_list_IL4_DESeq2_50.RData")


sig_gse_df_head <- head(sig_gse_df[,c("Description", "setSize", "p.adjust", "enrichmentScore")], 100)

GO_list_IL4_DESeq2_100 <- sig_gse_df_head$Description
length(GO_list_IL4_DESeq2_100)
save(GO_list_IL4_DESeq2_100, file="GO_list_IL4_DESeq2_100.RData")

# all

GO_list_IL4_DESeq2_all <- sig_gse_df$Description
length(GO_list_IL4_DESeq2_all)
save(GO_list_IL4_DESeq2_all, file="GO_list_IL4_DESeq2_all.RData")
```

##Dotplot
```{r echo=TRUE, fig.width=15, fig.height=8}
#require(DOSE)
#dotplot(gse_old, showCategory=10, split=".sign") + facet_grid(.~.sign)
```

```{r}
head(gse)
#barplot(gse, showCategory = 20)
```

##Dotplot
```{r echo=TRUE, fig.width=15, fig.height=8}
require(DOSE)
dotplot(gse, showCategory=50, split=".sign") + facet_grid(.~.sign)
```

##Encrichment plot map:
Enrichment map organizes enriched terms into a network with edges connecting overlapping gene sets. In this way, mutually overlapping gene sets are tend to cluster together, making it easy to identify functional modules.


This function add similarity matrix to the termsim slot of enrichment result. Users can use the
‘method‘ parameter to select the method of calculating similarity. The Jaccard correlation coefficient(JC) is used by default, and it applies to all situations. When users want to calculate the
correlation between GO terms or DO terms, they can also choose "Resnik", "Lin", "Rel" or "Jiang"
(they are semantic similarity calculation methods from GOSemSim packages), and at this time,
the user needs to provide ‘semData‘ parameter, which can be obtained through godata function in
GOSemSim package.

Enrichment map plots (emaplot) are useful because they allow for
the organization of enriched sets into networks where edges reflect
the overlap between gene sets, so that overlapping gene sets tend to
cluster together


```{r echo=TRUE,fig.width=7, fig.height=7}
pair_gse <- enrichplot::pairwise_termsim(gse)
emapplot(pair_gse, showCategory = 20)

```

##Category Netplot
The cnetplot depicts the linkages of genes and biological concepts (e.g. GO terms or KEGG pathways) as a network (helpful to see which genes are involved in enriched pathways and genes that may belong to multiple annotation categories).
```{r fig.width=18}
# categorySize can be either 'pvalue' or 'geneNum'
cnetplot(gse, categorySize="pvalue", foldChange=gene_list, showCategory = 3)
```

## Ridgeplot
Helpful to interpret up/down-regulated pathways.
```{r fig.width=10, fig.height=5}

ridgeplot(gse, showCategory = 15) + labs(x = "enrichment distribution")
```

## GSEA Plot  
Traditional method for visualizing GSEA result.  
  
Params:  
**Gene Set** Integer. Corresponds to gene set in the gse object. The first gene set is 1, second gene set is 2, etc. 

```{r fig.height=6}
# Use the `Gene Set` param for the index in the title, and as the value for geneSetId
gseaplot(gse, by = "all", title = gse$Description[1], geneSetID = 1)
```

## PubMed trend of enriched terms
Plots the number/proportion of publications trend based on the query result from PubMed Central.
```{r fig.width=10}
terms <- gse$Description[1:10]
pmcplot(terms, 2010:2021, proportion=FALSE)
```


# KEGG Gene Set Enrichment Analysis
For KEGG pathway enrichment using the `gseKEGG()` function, we need to convert id types. We can use the `bitr` function for this (included in clusterProfiler). It is normal for this call to produce some messages / warnings. 

In the `bitr` function, the param `fromType` should be the same as `keyType` from the `gseGO` function above (the annotation source). This param is used again in the next two steps: creating `dedup_ids` and `df2`.  

`toType` in the `bitr` function has to be one of the available options from `keyTypes(org.Dm.eg.db)` and must map to one of 'kegg', 'ncbi-geneid', 'ncib-proteinid' or 'uniprot' because `gseKEGG()` only accepts one of these 4 options as it's `keytype` parameter. In the case of org.Dm.eg.db, none of those 4 types are available, but 'ENTREZID' are the same as ncbi-geneid for org.Dm.eg.db so we use this for `toType`. 

As our intial input, we use `original_gene_list` which we created above.

## Prepare Input
```{r}
# Convert gene IDs for gseKEGG function
# We will lose some genes here because not all IDs will be converted
ids<-bitr(names(original_gene_list), fromType = "SYMBOL", toType = "ENTREZID", OrgDb=organism)

# remove duplicate IDS (here I use "ENSEMBL", but it should be whatever was selected as keyType)
dedup_ids = ids[!duplicated(ids[c("SYMBOL")]),]

# Create a new dataframe df2 which has only the genes which were successfully mapped using the bitr function above
df2 = df[df$X %in% dedup_ids$SYMBOL,]

# Create a new column in df2 with the corresponding ENTREZ IDs
df2$Y = dedup_ids$ENTREZID
head(df2)
# Create a vector of the gene unuiverse
kegg_gene_list <- df2$log2FoldChange

# Name vector with ENTREZ ids
names(kegg_gene_list) <- df2$Y

# omit any NA values 
kegg_gene_list<-na.omit(kegg_gene_list)

# sort the list in decreasing order (required for clusterProfiler)
kegg_gene_list = sort(kegg_gene_list, decreasing = TRUE)



```
## Create gseKEGG object
 
**organism** KEGG Organism Code: The full list is here: https://www.genome.jp/kegg/catalog/org_list.html (need the 3 letter code). I define this as `kegg_organism` first, because it is used again below when making the pathview plots.  
**nPerm** permutation numbers, the higher the number of permutations you set, the more accurate your results is, but it will also cost longer time for running permutation.  
**minGSSize** minimal size of each geneSet for analyzing.   
**maxGSSize** maximal size of genes annotated for testing.   
**pvalueCutoff** pvalue Cutoff.   
**pAdjustMethod** one of "holm", "hochberg", "hommel", "bonferroni", "BH", "BY", "fdr", "none".  
**keyType** one of 'kegg', 'ncbi-geneid', 'ncib-proteinid' or 'uniprot'.  
```{r}
kegg_organism = "mmu"
kk2 <- gseKEGG(geneList     = kegg_gene_list,
               organism     = kegg_organism,
               nPermSimple = 10000, #nPerm        = 10000,
               minGSSize    = 3,
               maxGSSize    = 800,
               pvalueCutoff = 0.05,
               pAdjustMethod = "none",
               keyType       = "ncbi-geneid")
```

```{r}

kegg_list = read.csv("C:/Lijuan/rattei_bioinf_master/_Diplome_FH/copy_files_from_server/SFB-RNA-data/DESeq2_edger_limma/kegg_pathway_from_paper.csv", header=FALSE)
#head(kegg_list)

kk2_df <- as.data.frame(kk2)
head (kk2_df)
# Create a new dataframe df2 which has only the genes which were successfully mapped using the bitr function above
#paper_kegg = kk2_df[kk2_df$ID %in% kegg_list$V1,]
#paper_kegg

sig_kk2_df <- kk2_df[kk2_df$p.adjust <= 0.05, ]
str(sig_kk2_df)

head(sig_kk2_df[,c("Description", "setSize", "p.adjust", "enrichmentScore")], 30)
KEGG_list_IL4_DESeq2_full <- head(sig_kk2_df[,c("Description", "setSize", "p.adjust", "enrichmentScore")], 30)
KEGG_list_IL4_DESeq2 <- KEGG_list_IL4_DESeq2_full$Description
length(KEGG_list_IL4_DESeq2)
save(KEGG_list_IL4_DESeq2, file="KEGG_list_IL4_DESeq2.RData")
```

## Dotplot
```{r echo=TRUE, fig.width=13, fig.height=7}
dotplot(kk2, showCategory = 10, title = "Enriched Pathways" , split=".sign") + facet_grid(.~.sign)
```

## Encrichment plot map:
Enrichment map organizes enriched terms into a network with edges connecting overlapping gene sets. In this way, mutually overlapping gene sets are tend to cluster together, making it easy to identify functional modules.
```{r echo=TRUE,fig.width=10, fig.height=7}
pair_kk2 <- enrichplot::pairwise_termsim(kk2)
 emapplot(pair_kk2)
```

## Category Netplot:
The cnetplot depicts the linkages of genes and biological concepts (e.g. GO terms or KEGG pathways) as a network (helpful to see which genes are involved in enriched pathways and genes that may belong to multiple annotation categories).
```{r fig.width=12}
# categorySize can be either 'pvalue' or 'geneNum'
cnetplot(kk2, categorySize="pvalue", foldChange=gene_list)
```

## Ridgeplot
Helpful to interpret up/down-regulated pathways.
```{r fig.width=10, fig.height=5}
ridgeplot(kk2, showCategory = 15) + labs(x = "enrichment distribution")
```

# GSEA Plot  
Traditional method for visualizing GSEA result.  
  
Params:  
**Gene Set** Integer. Corresponds to gene set in the gse object. The first gene set is 1, second gene set is 2, etc. Default: 1  

```{r fig.height=6}
# Use the `Gene Set` param for the index in the title, and as the value for geneSetId
gseaplot(kk2, by = "all", title = kk2$Description[1], geneSetID = 1)
```

#Pathview
This will create a PNG and *different* PDF of the enriched KEGG pathway.  
  
Params:  
**gene.data** This is `kegg_gene_list` created above  
**pathway.id** The user needs to enter this. Enriched pathways + the pathway ID are provided in the gseKEGG output table (above).  
**species** Same as `organism` above in `gseKEGG`, which we defined as `kegg_organism`



```{r, message=F, warning=F, echo = TRUE}
library(pathview)

# Produce the native KEGG plot (PNG)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00190", species = kegg_organism)

# Produce a different plot (PDF) (not displayed here)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00190", species = kegg_organism, kegg.native = F)
```
```{r pressure, echo=TRUE, fig.cap="KEGG Native Enriched Pathway Plot", out.width = '100%'}
knitr::include_graphics("mmu00190.pathview.png")
```



```{r, message=F, warning=F, echo = TRUE}


# Produce the native KEGG plot (PNG)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00010", species = kegg_organism)

# Produce a different plot (PDF) (not displayed here)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00010", species = kegg_organism, kegg.native = F)
```
```{r pressure, echo=TRUE, fig.cap="KEGG Native Enriched Pathway Plot", out.width = '100%'}
knitr::include_graphics("mmu00010.pathview.png")
```


```{r, message=F, warning=F, echo = TRUE}


# Produce the native KEGG plot (PNG)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00020", species = kegg_organism)

# Produce a different plot (PDF) (not displayed here)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00020", species = kegg_organism, kegg.native = F)
```
```{r pressure, echo=TRUE, fig.cap="KEGG Native Enriched Pathway Plot", out.width = '100%'}
knitr::include_graphics("mmu00020.pathview.png")
```


```{r, message=F, warning=F, echo = TRUE}


# Produce the native KEGG plot (PNG)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00030", species = kegg_organism)

# Produce a different plot (PDF) (not displayed here)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00030", species = kegg_organism, kegg.native = F)
```
```{r pressure, echo=TRUE, fig.cap="KEGG Native Enriched Pathway Plot", out.width = '100%'}
knitr::include_graphics("mmu00030.pathview.png")
```


```{r, message=F, warning=F, echo = TRUE}


# Produce the native KEGG plot (PNG)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00330", species = kegg_organism)

# Produce a different plot (PDF) (not displayed here)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00330", species = kegg_organism, kegg.native = F)
```
```{r pressure, echo=TRUE, fig.cap="KEGG Native Enriched Pathway Plot", out.width = '100%'}
knitr::include_graphics("mmu00330.pathview.png")
```

```{r, message=F, warning=F, echo = TRUE}


# Produce the native KEGG plot (PNG)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00061", species = kegg_organism)

# Produce a different plot (PDF) (not displayed here)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00061", species = kegg_organism, kegg.native = F)
```
```{r pressure, echo=TRUE, fig.cap="KEGG Native Enriched Pathway Plot", out.width = '100%'}
knitr::include_graphics("mmu00061.pathview.png")
```



```{r, message=F, warning=F, echo = TRUE}


# Produce the native KEGG plot (PNG)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00071", species = kegg_organism)

# Produce a different plot (PDF) (not displayed here)
dme <- pathview(gene.data=kegg_gene_list, pathway.id="mmu00071", species = kegg_organism, kegg.native = F, low = list(gene = "red", cpd ="yellow"), mid = list(gene = "gray", cpd = "gray"), high = list(gene = "green", cpd = "blue"))
```
```{r pressure, echo=TRUE, fig.cap="KEGG Native Enriched Pathway Plot", out.width = '100%'}
knitr::include_graphics("mmu00071.pathview.png")
```

